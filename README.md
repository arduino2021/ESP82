# ESP82-USB-V   电压电流表

#### 介绍
基于ESP8285 构建的一个USB TYPE-C 接口电压电流表，拥有高精度电压电流测量，串口下载通信等功能
使用ESP8285 WIFI芯片作主控  通过INA226 采集电压电流  0.96TFT显示  CH340E下载烧录  ME3116 DC降压供电

#### 软件架构
1.基于Arduino 框架开发 
2.目前只有电压电流检测显示 等基础功能
3.建议使用2.1最新版程序 安装好对应的库 
4.上传程序时必须按P0按键手动让P0脚拉低 CH340E芯片会自动复位ESP8285 从而进入下载模式

#### 硬件架构
1.TFT：显示屏为0.96寸 80*160像素 彩屏  

2.串口通信：板载CH340CE串口芯片  共用测量的TYPE-C接口 可以通过软件控制串口电源开关

3.程序下载：当程序上传编译完成进入下载模式时  按下P0按键并成功进入烧录模式 才能正常下载程序

4.电流电压检测：使用INA226芯片搭配0.01R电阻可以实现4-28V、0-8A的电压电流检测并且电流精度可达（0.001A）

5.WIFI通信： 未开发  

6.PD诱骗：FUSB302驱动不会搞  未开发对应功能 可以不焊

7.外壳：使用PCB外壳 + M2 *3铜柱  M2*2  M3*5平头螺丝 组成   PCB外壳需要手动裁切

8.注意：元件参数以原理图标注参数为准 未使用相关功能器件可以不焊   慎用附件内立创EDA导出的AD文件

#### 问题
1. ESP8285发热感人  导致NTC无法长时间测环境温度
2. 当插入被测设备瞬间掉电 有一定几率死机   

#### 参与贡献
1.  熵 2022 5 14

#### 立创开源链接
1.https://oshwhub.com/FJ956391150/esp82-usb-v

### 图片展示
![正面](https://images.gitee.com/uploads/images/2022/0514/181813_adb13e2a_8888576.jpeg "2fc48c43d125b5b94a3af1747f4aa56.jpg")
![背面](https://images.gitee.com/uploads/images/2022/0514/181848_7bb50ab4_8888576.jpeg "b0c6c168faac5940096bfbad54845af.jpg")
![](https://images.gitee.com/uploads/images/2022/0514/182030_0ecfb8a0_8888576.jpeg "2f41901785b5bf2ddad41444ee82612.jpg")
![](https://images.gitee.com/uploads/images/2022/0514/182054_a44c98ca_8888576.jpeg "176b16d83874d94acdd61ed8b0b2c76.jpg")
![3D](https://images.gitee.com/uploads/images/2022/0514/182114_e74dd0d4_8888576.png "157ae1460cae6e06a7f372791139bef.png")

